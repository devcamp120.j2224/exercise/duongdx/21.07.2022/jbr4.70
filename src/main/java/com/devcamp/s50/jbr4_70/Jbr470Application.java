package com.devcamp.s50.jbr4_70;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr470Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr470Application.class, args);
	}

}
