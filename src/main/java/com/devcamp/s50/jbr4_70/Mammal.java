package com.devcamp.s50.jbr4_70;

public class Mammal extends Animal{

    public Mammal(String name) {
        super(name);
        //TODO Auto-generated constructor stub
    }

    @Override
    public String toString() {
        return "Mammal [Animal [name=" + super.getName() + "]]";
    }

    public void greets(){
        System.out.println("Mammal Keu...");
    }
    
}
