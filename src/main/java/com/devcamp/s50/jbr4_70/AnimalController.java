package com.devcamp.s50.jbr4_70;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnimalController {
    @CrossOrigin
    @GetMapping("/cats")
    public ArrayList<Cat> getCatApi(){
        Cat cat1 = new Cat("Lila");
        Cat cat2 = new Cat("baba");
        Cat cat3 = new Cat("sasa");

        System.out.println(cat1);
        System.out.println(cat2);
        System.out.println(cat3);

        cat1.greets();

        ArrayList<Cat> ListCat = new ArrayList<>();
        ListCat.add(cat1);
        ListCat.add(cat2);
        ListCat.add(cat3);
        return ListCat ;
    }

    @CrossOrigin
    @GetMapping("/dogs")
    public ArrayList<Dog> getDogApi(){
        Dog dog1 = new Dog("Cun");
        Dog dog2 = new Dog("muc");
        Dog dog3 = new Dog("vang");

        System.out.println(dog1);
        System.out.println(dog2);
        System.out.println(dog3);

        dog1.greets();
        dog2.greets(dog3);

        ArrayList<Dog> dogsList = new ArrayList<>();
        dogsList.add(dog1);
        dogsList.add(dog2);
        dogsList.add(dog3);

        return dogsList;
    }
}
